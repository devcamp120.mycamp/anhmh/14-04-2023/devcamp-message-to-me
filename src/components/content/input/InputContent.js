import { Button, Col, Input, Label, Row } from "reactstrap";

function InputContent({inputMessageProps, inputMessageChangeHandlerProps, outputMessageChangeHandlerProps}) {
    const InputChangeHandler = (event) => {
        let value = event.target.value;
        console.log(value);
        inputMessageChangeHandlerProps(value);
    }

    const ButtonClickHandler = () => {
        console.log("Ấn nút gửi thông điệp!");
        outputMessageChangeHandlerProps();
    }

    return (
        <>
            <Row className="mt-5">
                <Col>
                    <Label>Message cho bạn 12 tháng tới</Label>
                    <Input onChange={InputChangeHandler} value={inputMessageProps} placeholder="Nhập message ở đây" style={{ width: "600px", margin: "0 auto" }}> </Input>
                </Col>
            </Row>
            <Button className="mt-3" color="primary" size="sm" onClick={ButtonClickHandler}>Gửi thông điệp</Button>
        </>

    )
}

export default InputContent;