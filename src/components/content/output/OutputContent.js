import like from '../../../assets/images/like-yellow-icon.png'

function OutputContent({outputMessageProps, likeDisplayProps}) {
    return (
        <>
            {outputMessageProps.map((element, index) => {
                return <p key={index}>{element}</p>
            })}

            <div className="row mt-2">
                <div className="col-12">

                </div>
            </div>
            <div className="row mt-2">
                <div className="col-12">
                    {likeDisplayProps ? <img src={like} alt="Chào mừng bạn đến với Devcamp" width="100px"></img> : <> </>}
                </div>
            </div>
        </>
    )
}

export default OutputContent;