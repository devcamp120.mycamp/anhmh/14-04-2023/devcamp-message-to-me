import { useState } from "react";
import InputContent from "./input/InputContent";
import OutputContent from "./output/OutputContent";
function Content() {
    const [inputMessage, setInputMessage] = useState("");
    const [outputMessage, setOnputMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState(false);

    const inputMessageChangeHandler = (value) => {
        setInputMessage(value);
    }

    const outputMessageChangeHandler = () => {
        if (inputMessage) {
            setOnputMessage ([...outputMessage, inputMessage]),
            setLikeDisplay (true)
        }
    }

    return (
        <>
            <InputContent inputMessageProps={inputMessage} inputMessageChangeHandlerProps={inputMessageChangeHandler} outputMessageChangeHandlerProps={outputMessageChangeHandler} />
            <OutputContent outputMessageProps={outputMessage} likeDisplayProps={likeDisplay} />
        </>
    )
}

export default Content;