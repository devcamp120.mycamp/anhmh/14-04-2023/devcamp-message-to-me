import ImageHeader from "./image/ImageHeader";
import TextHeader from "./text/TextHeader";

function Header() {
    return (
        <>
            <TextHeader />
            <ImageHeader />
        </>
    )
}
export default Header;