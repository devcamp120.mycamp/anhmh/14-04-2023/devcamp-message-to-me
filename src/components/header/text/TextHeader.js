import { Component } from "react";
import { Col, Row } from "reactstrap";

class TextHeader extends Component {
    render() {
        return (
            <Row>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <h1>Chào mừng đến với Devcamp 120</h1>
                </Col>
            </Row>
        )
    }
}

export default TextHeader;